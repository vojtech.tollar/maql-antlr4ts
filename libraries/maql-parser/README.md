# MAQL Parser library

A simple library that allows parsing of MAQL queries into AST nodes.

This library uses an [ANTLR](https://www.antlr.org/) grammar description to generate the parser and the lexer for MAQL.

## How to build

```bash
$ gw clean build
```

## How to use

Simply include this library to your microservice dependencies, like so:
```groovy
implementation(project(':libraries:maql-parser'))
```

After that you can use the library via its `ParserService` interface, like so:
```kotlin
package com.gooddata.tiger

import com.gooddata.tiger.maql.parser.ast.AstNode
import com.gooddata.tiger.maql.parser.service.ParserService

fun main() {
    // root level AST node for the given query
    val maqlAst: AstNode = ParserService.parse("""PARSE METRIC SELECT RUNSUM(SELECT "💩");""")
}
```
