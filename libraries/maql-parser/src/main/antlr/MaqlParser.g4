parser grammar MaqlParser;

options {
	tokenVocab = MaqlLexer ;
}

maql                : PARSE METRIC metricSelect SEMICOLON* EOF      #maqlMetricSelect
                    | PARSE FILTER filterExpression SEMICOLON* EOF  #maqlFilterExpression
                    ;

metricSelect        : SELECT metricExpression by? forTransformation? whereHaving? using? withParentFilter? ;

metricOpExpression  : opSubExprOrMul addOpExpr?
                    ;

addOpExpr           : (addOp opSubExprOrMul)+
                    ;

opSubExprOrMul      : metricOpSubExpression mulOpExpr?
                    ;

mulOpExpr           : (mulOp  metricOpSubExpression)+
                    ;

metricOpSubExpression : LPAREN metricExpression RPAREN      #metricExpressionParentheses
                      | LPAREN metricSelect RPAREN          #metricExpressionMetricSelect
                      | MINUS metricExpression              #metricExpressionNegation
                      | object                              #metricExpressionObject
                      | literal                             #metricExpressionLiteral
                      | metricFunction                      #metricExpressionFunction
                      ;

literal : unumber
        | string
        ;

metricExpression : metricOpExpression ;
metricFunction : COUNT LPAREN object RPAREN                                                  #metricExpressionCountObject
               | COUNT LPAREN object COMMA object RPAREN                                     #metricExpressionCountObjects
               | runfuncname LPAREN functionArgument RPAREN within? rollWindow?              #metricExpressionRunFunction
               | funcname1 LPAREN functionArgument RPAREN                                    #metricExpressionFunction1
               | funcname2 LPAREN metricExpression COMMA metricExpression RPAREN             #metricExpressionFunction2
               | funcname12 LPAREN functionArgument RPAREN                                   #metricExpressionFunction12Arg
               | funcname12 LPAREN metricExpression COMMA metricExpression RPAREN            #metricExpressionFunction12Metric
               | funcnamen LPAREN metricList COMMA metricExpression RPAREN                   #metricExpressionFunctionN
               | timeMacro LPAREN granularity RPAREN                                         #metricExpressionTimeMacro1
               | timeMacro LPAREN granularity COMMA integer RPAREN                           #metricExpressionTimeMacro2
               | IFNULL LPAREN metricExpression COMMA number RPAREN                          #metricExpressionIfNull
               | REPORT METRIC LPAREN posinteger RPAREN                                      #metricExpressionReport
               | CASE whenExpressions END                                                    #metricExpressionCase
               | CASE whenExpressions ELSE metricExpression END                              #metricExpressionCaseElse
               | IF filterExpression THEN metricExpression END                               #metricExpressionIf
               | IF filterExpression THEN metricExpression ELSE metricExpression END         #metricExpressionIfElse
               | PERCENTILE LPAREN metricExpression COMMA percentileValue RPAREN             #metricExpressionPercentile
               | rankfuncname LPAREN metricList RPAREN rankDir? within?                      #metricExpressionRankFunction
               ;

functionArgument    : metricExpression
                    | metricSelect
                    ;

within              : WITHIN LPAREN aggregations RPAREN                #withinAggregations
                    | WITHIN LPAREN aggregations allOther RPAREN       #withinAggregationsAllOther
                    | WITHIN LPAREN aggregations COMMA allOther RPAREN #withinAggregationsAllOther
                    | WITHIN LPAREN allOther RPAREN                    #withinAllOther
                    | WITHIN LPAREN CURRENT RPAREN                     #withinCurrent
                    ;

rollWindow          : ROWS BETWEEN windowBound AND windowBound  #rollWindowSet
                    ;

metricList          : metricExpression ( COMMA metricExpression )* ;

object              : TYPED_IDENTIFIER ;

by                  : BY aggregations                   #byAggregations
                    | BY aggregations allOther          #byAggregationsAllOther
                    | BY aggregations COMMA allOther    #byAggregationsAndAllOther
                    | BY allOther                       #byAllOther
                    ;

forTransformation   : FOR transformation    #forTransformationIncluded
                    ;

whereHaving         : WHERE filterExpression    #whereFilterExpression
                    | HAVING filterExpression   #havingFilterExpression
                    ;

using               : USING objects     #usingObjects
                    ;

withParentFilter    : WITH PF EXCEPT objects                #withPfExceptObjects
                    | WITH PARENT FILTER EXCEPT objects     #withPfExceptObjects
                    | WITH PF                               #withPf
                    | WITH PARENT FILTER                    #withPf
                    | WITHOUT PF EXCEPT objects             #withoutPfExceptObjects
                    | WITHOUT PARENT FILTER EXCEPT objects  #withoutPfExceptObjects
                    | WITHOUT PF                            #withoutPf
                    | WITHOUT PARENT FILTER                 #withoutPf
                    ;

aggregations        : aggregation ( COMMA aggregation )* ;

allOther            : ALL OTHER except?
                    | ALL IN ALL OTHER DIMENSIONS except?
                    ;

transformation      : transFunction LPAREN object RPAREN                #transformationWithObject
                    | transFunction LPAREN object COMMA integer RPAREN  #transformationWithObjectAndInteger
                    ;


filterExpression        : expressionItemAnd orOpExpression?
                        ;

orOpExpression          : (OR expressionItemAnd)+
                        ;

expressionItemAnd       : filterExpressionItem andOpExpression?
                        ;

andOpExpression         : (AND filterExpressionItem)+
                        ;

filterExpressionItem    : object                                                                      #filterExpressionObject
                        | LPAREN filterExpression RPAREN                                              #filterExpressionParentheses
                        | TRUE                                                                        #filterExpressionTrue
                        | FALSE                                                                       #filterExpressionFalse
                        | metricExpression BETWEEN metricExpression AND metricExpression              #filterExpressionMetricBetween
                        | metricExpression NOT BETWEEN metricExpression AND metricExpression          #filterExpressionMetricNotBetween
                        | LPAREN filterExpression RPAREN OVER object TO object                        #filterExpressionOver
                        | NOT filterExpressionItem                                                    #filterExpressionNot
                        | metricExpression RELOP metricExpression                                     #filterExpressionRelop
                        | metricExpression IN LPAREN metricList RPAREN                                #filterExpressionMetricIn
                        | metricExpression NOT IN LPAREN metricList RPAREN                            #filterExpressionMetricNotIn
                        | topBottom LPAREN topSpec RPAREN IN LPAREN functionArgument RPAREN within?   #filterExpressionTopBottomIn
                        | topBottom LPAREN topSpec RPAREN OF LPAREN metricList RPAREN within?         #filterExpressionTopBottomOf
                        | metricExpression LIKE string                                                #filterExpressionLike
                        | metricExpression ILIKE string                                               #filterExpressionILike
                        | metricExpression NOT LIKE string                                            #filterExpressionNotLike
                        | metricExpression NOT ILIKE string                                           #filterExpressionNotILike
                        ;

objects             : object ( COMMA object )* ;

aggregation         : object        #aggregationObject
                    | ALL object    #aggregationAll
                    ;

except              : EXCEPT objects        #exceptObjects
                    | EXCEPT FOR objects    #exceptObjects
                    ;

transFunction       : NEXT
                    | PREVIOUS
                    | NEXTPERIOD
                    | PREVIOUSPERIOD
                    ;

integer             : uinteger            #integerPos
                    | MINUS uinteger      #integerNeg
                    | PLUS uinteger       #integerPosWithSign // isn't overriden in MaqlAstVisitor -> PLUS gets ignored
                    ;

uinteger            : ZERO          #uintegerZero
                    | posinteger    #uintegerPos
                    ;

ureal               : REAL
                    ;

posinteger          : NATURAL ;

topBottom           : TOP
                    | BOTTOM
                    ;

topSpec             : unumber       #topSpecUnumber
                    | percentage    #topSpecPercentage
                    ;

string              : STRING ;

windowBound         : windowPreceding
                    | windowFollowing
                    | currentRow
                    ;

unumber             : uinteger | ureal;

percentage          : unumber PERCENT ;

number              : unumber           #numberPos
                    | MINUS unumber     #numberNeg
                    ;

timeMacro           : THIS
                    | NEXT
                    | PREVIOUS
                    ;

granularity         : DAY
                    | DAYOFWEEK
                    | DAYOFMONTH
                    | DAYOFYEAR
                    | HOUR
                    | HOUROFDAY
                    | MINUTE
                    | MINUTEOFHOUR
                    | MONTH
                    | MONTHOFYEAR
                    | QUARTER
                    | QUARTEROFYEAR
                    | YEAR
                    | WEEK
                    | WEEKOFYEAR
                    ;

whenExpressions     : whenExpression (COMMA whenExpression)* ;

rankDir             : ASC
                    | DESC
                    ;

percentileValue     : percentage
                    | unumber
                    ;

windowPreceding     : UNBOUNDED PRECEDING   #windowPrecedingUnbounded
                    | uinteger PRECEDING    #windowPrecedingBounded
                    ;

windowFollowing     : UNBOUNDED FOLLOWING   #windowFollowingUnbounded
                    | uinteger FOLLOWING    #windowFollowingBound
                    ;

currentRow          : CURRENT ROW ;

whenExpression      : WHEN filterExpression THEN metricExpression ;

funcname1           : SUM | MIN | MAX | AVG | MEDIAN | VAR | VARP | STDEV | STDEVP | EXP | LN | SQRT
                    | ABS | SIGN | FLOOR | CEIL | CEILING | APPROXIMATE_COUNT
                    ;

funcname2           : CORREL | COVAR | COVARP | RSQ | SLOPE | INTERCEPT | POWER ;

runfuncname         : RUNSUM | RUNMIN | RUNMAX | RUNAVG | RUNVAR | RUNVARP | RUNSTDEV | RUNSTDEVP ;

funcname12          : ROUND | TRUNC | LOG | FORECAST;

funcnamen           : GREATEST | LEAST ;

rankfuncname        : ROW_NUMBER | RANK | DENSE_RANK | PERCENT_RANK | CUME_DIST ;

addOp               : PLUS | MINUS ;
mulOp               : MUL | DIV ;
