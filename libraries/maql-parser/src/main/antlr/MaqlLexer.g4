lexer grammar MaqlLexer;

fragment A : [aA];
fragment B : [bB];
fragment C : [cC];
fragment D : [dD];
fragment E : [eE];
fragment F : [fF];
fragment G : [gG];
fragment H : [hH];
fragment I : [iI];
fragment J : [jJ];
fragment K : [kK];
fragment L : [lL];
fragment M : [mM];
fragment N : [nN];
fragment O : [oO];
fragment P : [pP];
fragment Q : [qQ];
fragment R : [rR];
fragment S : [sS];
fragment T : [tT];
fragment U : [uU];
fragment V : [vV];
fragment W : [wW];
fragment X : [xX];
fragment Y : [yY];
fragment Z : [zZ];

fragment WS_ : [\u0000-\u0020]+;

ABS             : A B S ;
ALL             : A L L ;
AND             : A N D ;
APPROXIMATE_COUNT: A P P R O X I M A T E '_' C O U N T ;
ASC             : A S C ;
ATTRIBUTE       : A T T R I B U T E ;
AVG             : A V G ;
BETWEEN         : B E T W E E N ;
BOTTOM          : B O T T O M ;
BY              : B Y ;
CASE            : C A S E ;
CEIL            : C E I L ;
CEILING         : C E I L I N G ;
CORREL          : C O R R E L ;
COUNT           : C O U N T ;
COVAR           : C O V A R ;
COVARP          : C O V A R P ;
CUME_DIST       : C U M E '_' D I S T ;
CURRENT         : C U R R E N T ;
DENSE_RANK      : D E N S E '_' R A N K ;
DESC            : D E S C ;
DIMENSIONS      : D I M E N S I O N S ;
ELSE            : E L S E ;
END             : E N D ;
EXCEPT          : E X C E P T ;
EXP             : E X P ;
FACT            : F A C T ;
FALSE           : F A L S E ;
FILTER          : F I L T E R ;
FLOOR           : F L O O R ;
FOLLOWING       : F O L L O W I N G ;
FORECAST        : F O R E C A S T ;
FOR             : F O R ;
GRAIN           : G R A I N ;
GREATEST        : G R E A T E S T ;
HAVING          : H A V I N G ;
IF              : I F ;
IFNULL          : I F N U L L ;
ILIKE           : I L I K E ;
IN              : I N ;
INTERCEPT       : I N T E R C E P T ;
LABEL           : L A B E L ;
LEAST           : L E A S T ;
LIKE            : L I K E ;
LN              : L N ;
LOCAL           : L O C A L ;
LOG             : L O G ;
MAX             : M A X ;
MEDIAN          : M E D I A N ;
METRIC          : M E T R I C ;
MIN             : M I N ;
NEXT            : N E X T ;
NEXTPERIOD      : N E X T P E R I O D ;
NOT             : N O T ;
OF              : O F ;
OR              : O R ;
OTHER           : O T H E R ;
OVER            : O V E R ;
PARENT          : P A R E N T ;
PARSE           : P A R S E ;
PERCENTILE      : P E R C E N T I L E ;
PERCENT_RANK    : P E R C E N T '_' R A N K ;
PF              : P F ;
POWER           : P O W E R ;
PRECEDING       : P R E C E D I N G ;
PREVIOUSPERIOD  : P R E V I O U S P E R I O D ;
PREVIOUS        : P R E V I O U S ;
PROMPT          : P R O M P T ;
RANK            : R A N K ;
REPORT          : R E P O R T ;
ROUND           : R O U N D ;
ROW_NUMBER      : R O W '_' N U M B E R ;
ROW             : R O W ;
ROWS            : R O W S ;
RSQ             : R S Q ;
RUNAVG          : R U N A V G;
RUNMAX          : R U N M A X ;
RUNMIN          : R U N M I N ;
RUNSUM          : R U N S U M ;
RUNSTDEV        : R U N S T D E V ;
RUNSTDEVP       : R U N S T D E V P ;
RUNVAR          : R U N V A R ;
RUNVARP         : R U N V A R P ;
SELECT          : S E L E C T ;
SIGN            : S I G N ;
SLOPE           : S L O P E ;
SQRT            : S Q R T ;
STDEV           : S T D E V ;
STDEVP          : S T D E V P ;
SUM             : S U M ;
THEN            : T H E N ;
THIS            : T H I S ;
TOP             : T O P ;
TO              : T O ;
TRUE            : T R U E ;
TRUNC           : T R U N C ;
UNBOUNDED       : U N B O U N D E D ;
USING           : U S I N G ;
VAR             : V A R ;
VARP            : V A R P ;
WHEN            : W H E N ;
WHERE           : W H E R E ;
WITHIN          : W I T H I N ;
WITHOUT         : W I T H O U T ;
WITH            : W I T H ;

// date granularities
DAY           : D A Y ;
DAYOFWEEK     : D A Y O F W E E K ;
DAYOFMONTH    : D A Y O F M O N T H ;
DAYOFYEAR     : D A Y O F Y E A R ;
HOUR          : H O U R ;
HOUROFDAY     : H O U R O F D A Y ;
MINUTE        : M I N U T E ;
MINUTEOFHOUR  : M I N U T E O F H O U R ;
MONTH         : M O N T H ;
MONTHOFYEAR   : M O N T H O F Y E A R ;
QUARTER       : Q U A R T E R ;
QUARTEROFYEAR : Q U A R T E R O F Y E A R ;
YEAR          : Y E A R ;
WEEK          : W E E K ;
WEEKOFYEAR    : W E E K O F Y E A R ;

WS            : WS_ -> channel(HIDDEN) ;

// comments
LINE_COMMENT            : '#' ~[\r\n]* -> channel(HIDDEN) ;

// Matches '{fact | metric | attribute | label | prompt | local/anything-non-empty}'
TYPED_IDENTIFIER            : '{' (FACT|METRIC|ATTRIBUTE|LABEL|PROMPT|LOCAL) '/' (~[}]+) '}' ;
UNKNOWN_TYPED_IDENTIFIER    : '{' (~[}]+) '/' (~[}]+) '}' ;
BROKEN_IDENTIFIER           : '{' (~[}]+) '}' ;

// TODO detect length in Java code and then return SHORT_STRING (<= 255), STRING(<= 2048) or LONG_STRING
UNTERMINATED_STRING     : '"' (~[\\"]|('\\'.))* ;
STRING                  : UNTERMINATED_STRING '"' ;

RELOP           : '<>' | '!=' | '<' | '>' | '=' | '<=' | '>=' ;
COMMA           : ',' ;
SEMICOLON       : ';' ;
LPAREN          : '(' ;
RPAREN          : ')' ;
MINUS           : '-' ;
PERCENT         : '%' ;
PLUS            : '+' ;
MUL             : '*' ;
DIV             : '/' ;

ZERO            : '0'+ ;
NATURAL         : '0'* [1-9] [0-9]* ;
REAL            : [0-9]+(('.'[0-9]+(E[+-]?[0-9][0-9]?[0-9]?)?)|(E[+-]?[0-9][0-9]?[0-9]?)) ;

// TODO remove before putting to prod
PERL_LEXER_FUCKUP       : ('\\n' | '\\f' | '\\r' | '\\t' | '\\e' | '\\a' | '\\b') -> skip;

// Any undefined keyword
// This rule is in place to require spaces between keywords
// For reference: https://stackoverflow.com/questions/15503561/antlr4-whitespace-handling
UNDEFINED_KEYWORD : [a-zA-Z_][a-zA-Z_0-9-]* ;

// Any character which does not match one of the above rules will appear in the token stream as an ERROR_CHARACTER token.
// This ensures the lexer itself will never encounter a syntax error and recognition errors can be handled separately
ERROR_CHARACTER : . ;
