import commonjs from '@rollup/plugin-commonjs';
import dts from 'rollup-plugin-dts'

export default [
    {
        input: 'dist/index.js',
        output: {
            file: 'index.js',
            format: 'cjs',
            exports: 'auto',
        },
        external: [
            'antlr4ts/**/*',
        ],
        plugins: [commonjs()],
    },
    {
        input: 'dist/index.d.ts',
        output: {
            file: 'index.d.ts',
        },
        plugins: [dts()]
    }
]
