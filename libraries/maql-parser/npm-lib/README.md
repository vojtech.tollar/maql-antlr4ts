#MAQL Parser npm package

The purpose of this module is to generate a TypeScript lexer and parser
from the MAQL grammar and to publish those as an npm package to the
private GitLab repository.

##Publishing

Publishing of this package happens automatically after merging.
Unless you have a good reason to do so (i.e. you cannot test a feature otherwise)
**do not** publish the package manually.

##Steps

Follow these steps to manually publish the package.

Create a GitLab access token with `api` permissions and store it
in the `NPM_TOKEN` environment variable.

`export NPM_TOKEN=<your acces token>`

Publish the package. You must first set a new version of the package. You must choose one that is not yet taken
(or delete the existing package). Then you can publish using

`npm publish`

You should now be able to see the package in the GitLab project registry.

The id of the project is set to `16539767`


